$(document).ready(function () {
  $.Scrollax();

  // countdown timers in hero for events
  timer = (target, date) => {
    var target = document.querySelector(target);
    var countDownDate = new Date(date).getTime();

    setInterval(() => {
      var now = new Date().getTime();
      var distance = countDownDate - now;

      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);

      target.querySelector('.days').innerText = days;
      target.querySelector('.hours').innerText = hours;
      target.querySelector('.minutes').innerText = minutes;
      target.querySelector('.seconds').innerText = seconds;

      if (distance < 0) {
        target.innerText = "Expired";
      }
    }, 1000);
  };
  var timerValue = $('#timer1').attr('data-timer');
  timer("#timer1", timerValue);
  // /////////////////////
  var speakers_slider_options = {
    dots: false,
    infinite: false,
    arrows: true,
    speed: 600,
    slidesToShow: 5,
    slidesToScroll: 5,
    rtl: $('html').attr('dir') === 'rtl' ? true : false ,
    draggable: false,
    responsive: [

      {
        breakpoint: 1025,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
        }
        },

      {
        breakpoint: 769,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        }
        },
      {
        breakpoint: 426,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        }
      },
      {
        breakpoint: 376,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },

    ],
  };
  $speakers_slider.slick(speakers_slider_options);
});