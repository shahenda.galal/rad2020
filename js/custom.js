var Session = {
  //save an item to localStorage
  setItem: function(key, value) {
    return localStorage.setItem(key, JSON.stringify(value));
  },

  //retrieve an item from localStorage
  getItem: function(key) {
    if (localStorage.getItem(key) == undefined)
      return {};

    return JSON.parse(localStorage.getItem(key));
  },

  //remove one item from localStorage
  removeItem: function(key) {
    return Session.setItem(key, {});
  },

  //remove all items from localStorage
  clear: function() {
    localStorage.clear();
  }
};
var session = Session.getItem("session");
var lang = session.eventLangKey;



function arabicLayout() {
  $('html').attr('dir', 'rtl');
  $(document).find('.lang-btn a').addClass('hidden');
  $(document).find('.lang-btn .en').removeClass('hidden');
  $('link[href="css/rtl.min.css?v=1"]').prop('disabled', false);
  $('.register-link').attr('href','https://forms.gle/9tEsfqvAPVtgnUMk9');
  $('.iframe-src').attr('src','https://forms.gle/9tEsfqvAPVtgnUMk9');
  $('.invitation').hide();

};

function englishLayout() {
  $('html').attr('dir', 'ltr');
  $('link[href="css/rtl.min.css?v=1"]').prop('disabled', true);
  $('body .toLeft').addClass('toRight').removeClass('toLeft');
  $(document).find('.lang-btn a').addClass('hidden');
  $(document).find('.lang-btn .ar').removeClass('hidden');
  $('.princename').hide();

  // $('.hero-bg').css('background-image','url(assets/img/img_bg_en.png)');
  $('.about-img').attr('src','assets/img/about_en.png').attr('srcset','assets/img/about_en.png');
};
var checkLang = function() {
  // directions of document
  if (lang === 'ar') {
    arabicLayout();

  } else if (lang === 'en') {
    englishLayout();
  }
};
function doLocalize(lang) {
  session.eventLangKey = lang;
  Session.setItem("session", session);
};
function resizeCheck() {
  if($(window).width() < 769){
    window.disableOnScroll = 1;
    $('.princename').attr('src','assets/img/invitation_white.svg').attr('srcset','assets/img/invitation_white.svg');
    // if($('html').attr('dir') === 'rtl'){
    //   // $('.invitation').hide();
    // }else{
    //   // $('.princename').hide();
    //   $('.princename').attr('src','assets/img/princename_white.png').attr('srcset','assets/img/princename_white.png');
    // }
  }
  else{
    window.disableOnScroll = 0;
    $('.princename').attr('src','assets/img/invitation.svg').attr('srcset','assets/img/invitation.svg');

    // if($('html').attr('dir') === 'rtl'){

    //   // $('.princename').attr('src','assets/img/princename.png').attr('srcset','assets/img/princename.png') 
    // }else{
    //   // $('.princename').attr('src','assets/img/princename_en.png').attr('srcset','assets/img/princename_en.png');
    // }
  }

};
var $speakers_slider = $('.js-speakers-slider');

$(document).ready(function() {



 $(document).on('click','.lang-btn .ar', function(){
    lang = 'ar';
    doLocalize(lang);
    arabicLayout();
    $('.toLeft').addClass('toRight');
    $('html').removeClass('sidebarShown ');
    location.reload();
    return false;
  });
 $(document).on('click','.lang-btn .en', function(){
    lang = 'en';
    doLocalize(lang);
    englishLayout();
    $('html').removeClass('sidebarShown ');
    location.reload();
    return false;
  });
  // bg white for nav on scroll
  if (window.isScroll) {
    $(window).on('scroll', function() {
      if ($(this).scrollTop() >= 10) {
        $('body').addClass('notTop');

      } else {
        $('body').removeClass('notTop');
      }
    });
  }

  



  checkLang();
  resizeCheck();


  // tabs for events timeline 
  $('[data-tab]').on('click', function() {
    $(this).addClass('active').siblings('[data-tab]').removeClass('active');
    $(this).closest('.wrap').find('[data-content=' + $(this).data('tab') + ']').addClass('active').siblings('[data-content]').removeClass('active')
  })


  $('.register-block').on('click', function() {
    $('.iframe').addClass('showed');
  });
  $(document).on('click','.close-iframe', function() {
    $('.iframe').removeClass('showed');
  });

  // ////////////////////////////

});
$(window).resize(function() {
  $('html').removeClass('sidebarShown ');
  resizeCheck();
});
$(window).on('load', function() {
      if (lang === undefined) {
    lang = "ar";
    doLocalize(lang);
    $("[data-localize]").localize("language/lang", {
      language: session.eventLangKey ,

    });
    
  } else { 
    $("[data-localize]").localize("language/lang", {
      language: session.eventLangKey 
    });
  }
  $('.loader').fadeOut('slow');
  checkLang();
  $('.menu-links li a[href="' + location.pathname.split("/")[2] + '"]').closest('li').addClass('active');

});
